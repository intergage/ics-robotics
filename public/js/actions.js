function includeHTML() {
  var z, i, elmnt, file, xhttp;
  /* Loop through a collection of all HTML elements: */
  z = document.getElementsByTagName("*");
  for (i = 0; i < z.length; i++) {
    elmnt = z[i];
    /*search for elements with a certain atrribute:*/
    file = elmnt.getAttribute("data-include");
    if (file) {
      /* Make an HTTP request using the attribute value as the file name: */
      xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (this.readyState == 4) {
          if (this.status == 200) {
            elmnt.innerHTML = this.responseText;
          }
          if (this.status == 404) {
            elmnt.innerHTML = "Page not found.";
          }
          /* Remove the attribute, and call this function once more: */
          elmnt.removeAttribute("data-include");
          includeHTML();
        }
      };
      xhttp.open("GET", file, true);
      xhttp.send();
      generateMenu();
      /* Exit the function: */
      return;
    }
  }
}



function generateMenu() {
  $("#main-menu").mmenu(
    {
      extensions: ["position-right"]
    },
    {
      clone: true,
      offCanvas: {
        pageSelector: "#wrap"
      }
    }
  );

  $("#mm-main-menu")
    .removeClass("d-none")
    .removeClass("d-lg-block")
    .removeClass("main-menu");

  var api = $("#mm-main-menu").data("mmenu");

  $("#burger").click(function() {
    api.open();
  });
  $("#close").click(function() {
    api.close();
  });
}

includeHTML();